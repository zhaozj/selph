# Summary #
### A Prototype System for Research ###
This software has been developed for the following research paper: 

Yuki Koyama, Daisuke Sakamoto, and Takeo Igarashi. 2016. SelPh: Progressive Learning and Support of Manual Photo Color Enhancement. In Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (CHI '16), pp.XX--XX. (Accepted)

### Project Page ###
Please visit our project page for details: [http://koyama.xyz/project/SelPh/](http://koyama.xyz/project/SelPh/)

### Software Name ###
SelPh: A self-reinforcing photo color enhancement system.

### Language ###
C++11 is used for most parts. For real-time photo color enhancement, GLSL is used.

# Set Up #
### Mac OS X ###
This software has been written for and tested on Mac OS X (x86_64). Other platforms (e.g., Windows, Linux) are currently not supported. If you want to build this software on other platforms, you might need to modify several parts (e.g., header paths), and also need to build the third-party libraries by yourself.

### Qt ###
This software is developed using [Qt](http://www.qt.io/) and Qt Creator. You need to install Qt into your system in advance.

# License #
This software, except the third-party libraries, is released under the **MIT License**, see LICENSE.txt.

Under the MIT License, you can freely use our source codes in your commercial software (on your own responsibility), but we would appreciate it if you would contact us :-)

# Included Third-Parties #
* [Eigen](http://eigen.tuxfamily.org/) (MPL2)
* [nlopt](http://ab-initio.mit.edu/wiki/index.php/NLopt) (GNU LGPL)

# Test Photographs #
This repository includes a photo album for testing the software. These photographs were taken by Yuki Koyama, and licensed under [CC BY](https://creativecommons.org/licenses/by/4.0/). If you want higher resolution photographs, please contact us.

# Developer/Contact #
* [Yuki Koyama](http://koyama.xyz/) - [koyama@is.s.u-tokyo.ac.jp](mailto:koyama@is.s.u-tokyo.ac.jp)